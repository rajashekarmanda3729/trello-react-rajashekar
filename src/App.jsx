import React from 'react'
import { BrowserRouter, Routes, Route } from 'react-router-dom'
import Header from './components/Header'
import Boards from './components/Boards'
import BoardDetails from './components/BoardDetails'
import NotFound from './components/NotFound.jsx'
import './App.css'

function App() {

  return (
    <BrowserRouter>
      <Header />
      <Routes>
        <Route path='/' element={<Boards />} />
        <Route path='/boards/:boardId' element={<BoardDetails />} />
        <Route path='*' element={<NotFound />} />
      </Routes>
    </BrowserRouter>
  )
}

export default App
