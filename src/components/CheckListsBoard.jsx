import React from 'react';
import Button from 'react-bootstrap/Button';
import Modal from 'react-bootstrap/Modal';
import CheckList from './CheckList';
import { getChecklistsOfCard, createACheckList } from './Server';

function CheckListsBoard(props) {
    const [checkListsDataArr, setCheckListsDataArr] = React.useState([])
    const [checkListName, setCheckListName] = React.useState('')

    React.useEffect(() => {
        getChecklistsOfCard(props.id)
            .then((res) => {
                setCheckListsDataArr(res.data)
            })

    }, [])

    const onChangeCheckListName = (event) => setCheckListName(event.target.value)

    const onAddCheckList = () => {
        createACheckList(checkListName, props.id)
            .then(res => {
                setCheckListsDataArr(prev => {
                    return [...prev, res.data]
                })
                setCheckListName('')
            })
    }

    return (
        <Modal
            {...props}
            size="lg"
            aria-labelledby="contained-modal-title-vcenter"
            centered
        >
            <Modal.Header closeButton>
                <Modal.Title id="contained-modal-title-vcenter">
                    <span style={{ color: 'blue', fontSize: '2rem' }}>{props.name}</span>
                </Modal.Title>
            </Modal.Header>
            <Modal.Body>
                <div className='d-flex flex-row'>
                    <h2 className='btn btn-light'>Create a New List</h2>
                    <div className='d-flex flex-row m-1'>
                        <input type='text' onChange={onChangeCheckListName} value={checkListName} placeholder='enter new checklist name' />
                        <button type='button' className='btn btn-primary' onClick={onAddCheckList}>add Checklist</button>
                    </div>
                </div>
                <hr/>
                {
                    checkListsDataArr.length != 0 ? checkListsDataArr.map(each => {
                        return <CheckList key={each.id} checkListDetails={each} setCheckListsDataArr={setCheckListsDataArr} cardId={props.id} />
                    }) : <p>No Checklist's</p>
                }

            </Modal.Body>
            <Modal.Footer>
                <Button onClick={props.onHide}>Close</Button>
            </Modal.Footer>
        </Modal>
    );
}

export default CheckListsBoard