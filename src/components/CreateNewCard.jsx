import React from 'react';
import Button from 'react-bootstrap/Button';
import Modal from 'react-bootstrap/Modal';
import { createNewCardAPI } from '../components/Server'

function CreateNewCard({ setShowCard, setListCardsList, listId }) {

    const [cardName, setCardName] = React.useState('')
    const closeCard = () => setShowCard(false)

    const onChangeCardName = (event) => setCardName(event.target.value)

    const onAddCard = () => {
        createNewCardAPI(listId, cardName)
            .then(res => {
                setListCardsList(prev => {
                    return [...prev, res.data]
                })
                setCardName('')
                setShowCard(false)
            })
    }

    return (
        <div
            className="modal show d-flex p-1"
            style={{ display: 'block', position: 'initial' }}
        >
            <Modal.Dialog style={{ boxShadow: '0px 0px 10px 2px gray' }}>
                <Modal.Body>
                    <input type='textarea' style={{ width: '15rem', height: '50px', fontSize: '1.4rem' }} onChange={onChangeCardName} placeholder='enter board name' />
                    <div className='d-flex flex-row justify-content-between mt-3'>
                        <Button variant="light" style={{ height: '34px', width: '150px' }} onClick={onAddCard}>Add card</Button>
                        <Modal.Header closeButton style={{ height: '34px', width: '60px' }} onClick={closeCard}></Modal.Header>
                    </div>
                </Modal.Body>
            </Modal.Dialog>
        </div>
    );
}

export default CreateNewCard;