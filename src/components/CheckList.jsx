import React from 'react'
import { deleteCheckListAPI, getCheckItemsOfCheckList, createCheckItemOfCheckList } from './Server'
import CheckItem from './CheckItem'

const CheckList = ({ checkListDetails, setCheckListsDataArr, cardId }) => {
    const { name, id } = checkListDetails

    const [checkItems, setCheckItems] = React.useState([])
    const [checkItemName, setCheckItemName] = React.useState('')
    const [rangeValue, setRangeValue] = React.useState({ range: 0, total: checkItems.length })


    React.useEffect(() => {
        let checkedCount = 0
        checkItems.map(each => {
            if (each.state == 'complete') {
                checkedCount++
            }
        })
        setRangeValue(prev => {
            return { range: checkedCount, total: checkItems.length }
        })
    }, [checkItems])

    React.useEffect(() => {
        getCheckItemsOfCheckList(id)
            .then(res => {
                setCheckItems(res.data)
            })
    }, [])


    const onAddCheckItem = () => {
        createCheckItemOfCheckList(id, checkItemName)
            .then(res => {
                setCheckItems(prev => {
                    return [...prev, res.data]
                })
                setCheckItemName('')
            })
    }

    const onChangeCheckItemName = (event) => setCheckItemName(event.target.value)
    

    const onDeleteCheckList = () => {
        deleteCheckListAPI(id)
            .then(res => {
                setCheckListsDataArr(prev => {
                    const filterData = prev.filter(each => each.id != id)
                    return filterData
                })
            })
    }

    return (
        <div className='d-flex flex-column '>
            <div className='d-flex flex-row align-items-center'>
                <h5 className='m-1'>{name}</h5>
                <button type='button' className='btn btn-light mb-3 mt-3 m-1' onClick={onDeleteCheckList}>delete</button>
            </div>
            <input type='range' value={`${rangeValue.range}`} min={0} max={rangeValue.total} />
            <div className='d-flex flex-column justify-content-evenly m-1'>
                {
                    checkItems.map(eachCheckItem => {
                        return <CheckItem checkItemDetails={eachCheckItem} key={eachCheckItem.id} setCheckItems={setCheckItems} checkListId={id} cardId={cardId} />
                    })
                }
                <div className='d-flex flex-row align-items-center'>
                    <input type='text' onChange={onChangeCheckItemName} value={checkItemName} />
                    <button type='button' className='btn btn-light' onClick={onAddCheckItem}>Add checkItem</button>
                </div>
            </div>
            <hr style={{borderWidth:'0.3rem',color:'black'}}/>
        </div>
    )
}

export default CheckList