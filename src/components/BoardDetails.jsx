import React from 'react'
import { useParams } from 'react-router'
import { getBoardListsAPI, getCardsOfBoardAPI } from './Server'
import ListItem from './ListItem'
import CreateNewList from './CreateNewList'
import BoardHeader from './BoardHeader'
import { getABoard } from '../components/Server.jsx'
import Spinner from 'react-bootstrap/Spinner';

const BoardDetails = () => {
    const { boardId } = useParams()

    const [boardListsData, setBoardListsData] = React.useState([])
    const [currentBoard, setCurrentBoard] = React.useState('')

    React.useEffect(() => {
        getABoard(boardId)
            .then(res => setCurrentBoard(res.data.name))
        getBoardListsAPI(boardId)
            .then(res => setBoardListsData(res.data))
    }, [])

    return (
        <div className='d-flex flex-column' style={{backgroundColor:'lightyellow'}}>
            <BoardHeader currentBoardName={currentBoard} />
            <div className='d-flex flex-row overflow-auto' style={{ height: '87vh' }}>
                {
                    boardListsData.map(eachList => {
                        return <ListItem key={eachList.id} listDetails={eachList} setBoardListsData={setBoardListsData} />
                    })
                }
                <CreateNewList setBoardListsData={setBoardListsData} />
            </div>
            <div className='d-flex flex-row justify-content-center align-items-center'>
                {boardListsData.length == 0 && <Spinner animation="border" size="xl" />}
            </div>
        </div>
    )
}

export default BoardDetails