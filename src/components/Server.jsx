import axios from "axios";

const KEY = 'e94df84a82a9d03ca8c741197932efd4'
const ACCESS_TOKEN = 'ATTA78d127946edd493602b647f55504494d894f03fc87ed05e9277beb1e9d072023789911ED'

export function getBoards() {
    return axios.get(`https://api.trello.com/1/members/me/boards?key=${KEY}&token=${ACCESS_TOKEN}`).then(res => res)
}

export function getABoard(boardId) {
    return axios.get(`https://api.trello.com/1/boards/${boardId}?key=${KEY}&token=${ACCESS_TOKEN}`)
}

export function getBoardCards(boardId) {
    return axios.get(`https://api.trello.com/1/boards/${boardId}/cards?key=${KEY}&token=${ACCESS_TOKEN}`)
}

export function createNewBoardAPI(name) {
    return axios.post(`https://api.trello.com/1/boards/?name=${name}&key=${KEY}&token=${ACCESS_TOKEN}`)
        .then(res => res.data)
}

export function deleteBoardAPI(id) {
    return axios.delete(`https://api.trello.com/1/boards/${id}?key=${KEY}&token=${ACCESS_TOKEN}`)
}

export function getBoardListsAPI(boardId) {
    return axios.get(`https://api.trello.com/1/boards/${boardId}/lists?key=${KEY}&token=${ACCESS_TOKEN}`)
}

export function createListToBoardAPI(name, boardId) {
    return axios.post(`https://api.trello.com/1/boards/${boardId}/lists?name=${name}&key=${KEY}&token=${ACCESS_TOKEN}`)
}

export function archiveListItemAPI(listId) {
    return axios.put(`https://api.trello.com/1/lists/${listId}/closed?value=true&key=${KEY}&token=${ACCESS_TOKEN}`)
}

export function getCardsOfBoardAPI(boardId) {
    return axios.get(`https://api.trello.com/1/boards/${boardId}/cards?key=${KEY}&token=${ACCESS_TOKEN}`)
}

export function createNewCardAPI(listId, cardName) {
    return axios.post(`https://api.trello.com/1/cards?name=${cardName}&idList=${listId}&key=${KEY}&token=${ACCESS_TOKEN}`)
}

export function deleteCardAPI(cardId) {
    return axios.delete(`https://api.trello.com/1/cards/${cardId}?key=${KEY}&token=${ACCESS_TOKEN}`)
}

export function getCardsInLists(listId) {
    return axios.get(`https://api.trello.com/1/lists/${listId}/cards?key=${KEY}&token=${ACCESS_TOKEN}`)
}

export function getChecklistsOfCard(cardId) {
    return axios.get(`https://api.trello.com/1/cards/${cardId}/checklists?key=${KEY}&token=${ACCESS_TOKEN}`)
}

export function createACheckList(checkListName, cardId) {
    return axios.post(`https://api.trello.com/1/checklists?name=${checkListName}&idCard=${cardId}&key=${KEY}&token=${ACCESS_TOKEN}`)
}

export function deleteCheckListAPI(checkListId) {
    return axios.delete(`https://api.trello.com/1/checklists/${checkListId}?key=${KEY}&token=${ACCESS_TOKEN}`)
}

export function getCheckItemsOfCheckList(checkListId) {
    return axios.get(`https://api.trello.com/1/checklists/${checkListId}/checkItems?key=${KEY}&token=${ACCESS_TOKEN}`)
}

export function createCheckItemOfCheckList(checkListId, checkItemName) {
    return axios.post(`https://api.trello.com/1/checklists/${checkListId}/checkItems?name=${checkItemName}&key=${KEY}&token=${ACCESS_TOKEN}`)
}

export function deleteCheckItemOnCheckList(checkListId, checkItemId) {
    return axios.delete(`https://api.trello.com/1/checklists/${checkListId}/checkItems/${checkItemId}?key=${KEY}&token=${ACCESS_TOKEN}`)
}

export function updateCheckItemOnCard(cardId, checkItemId, status) {
    return axios.put(`https://api.trello.com/1/cards/${cardId}/checkItem/${checkItemId}?state=${status}&token=${ACCESS_TOKEN}&key=${KEY}`)
}