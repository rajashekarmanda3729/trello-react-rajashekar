import React from 'react'
import { Link, useParams } from 'react-router-dom'
import logo from '../assets/Logo.png'
import homeImage from '../assets/icons8-home-64.png'
import boardsImage from '../assets/icons8-news-64.png'
import logoSideImage from '../assets/icons8-grid-view-64.png'


const Header = () => {
    return (
        <div className='d-flex flex-row justify-content-between align-items-center bg-primary' style={{ height: '4rem' }}>
            <div className='d-flex flex-row justify-content-between align-items-center' >
                <Link to='/' className='m-2'>
                    <img src={homeImage} alt='logo' style={{ backgroundColor: 'white', height: '40px' }} />
                </Link>
                <Link to='/' className='d-flex align-items-center m-2 p-1' style={{ color: 'black', height: '40px', fontSize: '32px', backgroundColor: 'white', textDecoration: 'none' }}>
                    <img src={boardsImage} alt='boards' style={{ height: '35px' }} className='m-1' /> Boards
                </Link>
            </div >
            <div>
                <img src={logo} alt='home' style={{ height: '35px' }} />
            </div>
            <form className="d-flex" role="search">
                <input
                    className="form-control me-2"
                    type="search"
                    placeholder="Search"
                    aria-label="Search"
                />
                <button className="btn btn-outline-success" type="submit">
                    Search
                </button>
            </form>
        </div >
    )
}

export default Header