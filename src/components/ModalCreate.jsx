import React, { useState } from 'react';
import Button from 'react-bootstrap/Button';
import Form from 'react-bootstrap/Form';
import Modal from 'react-bootstrap/Modal';

function ModalCreate({ setBoardName, createBoard, name, style, type }) {
  const [show, setShow] = useState(false);

  const handleClose = () => {
    createBoard()
    setShow(false)
  }
  const handleShow = () => setShow(true);


  const onChangeBoardName = (e) => setBoardName(e.target.value)


  return (
    <>
      <Button variant={style} onClick={handleShow}>
        {name}
      </Button>

      <Modal show={show} onHide={handleClose}>
        <Modal.Header closeButton>
          <Modal.Title>{type == 'board' ? 'Create Board' : 'Create List'}</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Form>
            <Form.Group className="mb-3" controlId="exampleForm.ControlInput1">
              <Form.Label>{type == 'board' ? 'Board Title' : 'List title'}</Form.Label>
              <Form.Control
                type="text"
                placeholder="enter board title"
                autoFocus
                onChange={onChangeBoardName}
              />
            </Form.Group>
            <Form.Group
              className="mb-3"
              controlId="exampleForm.ControlTextarea1"
            >
              <label htmlFor='selectWorkSpace'>Visibility</label>
              <br />
              <select id='selectWorkSpace'>
                <option>Workspace</option>
                <option>Public</option>
                <option>Private</option>
              </select>
            </Form.Group>
          </Form>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="primary" onClick={handleClose}>
            Create
          </Button>
        </Modal.Footer>
      </Modal>
    </>
  );
}

export default ModalCreate