import React from 'react'
import editIcon from '../assets/icons8-edit-48.png'
import Button from 'react-bootstrap/Button';
import OverlayTrigger from 'react-bootstrap/OverlayTrigger';
import Popover from 'react-bootstrap/Popover';
import { deleteCardAPI, getChecklistsOfCard } from './Server';
import Spinner from 'react-bootstrap/Spinner';
import CheckListsBoard from './CheckListsBoard'

const Card = ({ cardDetails, setListCardsList }) => {
    const { id, name } = cardDetails

    const [isLoading, setIsLoading] = React.useState(false)
    const [checkListModalShow, setCheckListModalShow] = React.useState(false);


    const deleteCard = () => {
        setIsLoading(true)
        deleteCardAPI(id)
            .then(res => {
                setListCardsList(prev => {
                    setIsLoading(false)
                    return prev.filter(eachCard => eachCard.id !== id)
                })
            })
    }

    return (
        <div className="card mt-2" style={{ width: "16rem", height: '2.3rem' }}>
            <div className="d-flex flex-row justify-content-between p-2">

                <h5 className="card-title" style={{ fontSize: "1rem", cursor: 'pointer' }} onClick={() => setCheckListModalShow(true)}>{name}</h5>
                <CheckListsBoard show={checkListModalShow} onHide={() => setCheckListModalShow(false)} name={name} id={id} />

                {
                    isLoading ?
                        <Spinner animation="border" size="sm" variant='danger' /> :
                        <OverlayTrigger
                            trigger="click"
                            key='right'
                            placement='right'
                            overlay={
                                <Popover id={`popover-positioned-'right'`}>
                                    <Popover.Header as="h3">{name}</Popover.Header>
                                    <Popover.Body>
                                        <div className='d-flex flex-column'>
                                            <button type='button' className='btn btn-light m-2'>@Edit Name</button>
                                            <button type='button' className='btn btn-light m-2'>@Archive</button>
                                            <button type='button' className='btn btn-light m-2'>Copy</button>
                                            <button type='button' className='btn btn-light m-2'>Move</button>
                                            <button type='button' className='btn btn-light m-2' onClick={deleteCard}>@Delete</button>
                                        </div>
                                    </Popover.Body>
                                </Popover>
                            }
                        >
                            <Button variant="light" style={{ height: '1.5rem', width: '1.5rem', padding: '2px' }}>
                                <img src={editIcon} alt='pen' style={{ width: '1rem', height: '1rem' }} className='mb-3' />
                            </Button>
                        </OverlayTrigger>
                }

            </div>
        </div>
    )
}

export default Card