import React from 'react'
import ModalCreate from './ModalCreate'


const CreateNewBoard = ({ createNewBoard, boardsData }) => {

    const [boardName, setBoardName] = React.useState('')

    const createBoard = () => createNewBoard(boardName)


    return (
        <div className="card text-bg-dark bg-light m-3 p-3" style={{ width: "18rem", height: '8.5rem' }}>
            <form >
                <div >
                    <h5 className="card-title" style={{ color: 'black' }}>{'Create new Board +'}</h5>
                    <p style={{ color: 'black' }} className='mt-2'>@ {10 - boardsData.length} Remaining</p>
                    <ModalCreate setBoardName={setBoardName} createBoard={createBoard} name={'Add +'} style={'primary'} type={'board'} />
                </div>
            </form>
        </div>
    )
}

export default CreateNewBoard