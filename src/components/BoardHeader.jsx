import Container from 'react-bootstrap/Container';
import Navbar from 'react-bootstrap/Navbar';

function BoardHeader({ currentBoardName }) {
    return (
        <Navbar expand="lg" variant="light" bg="secondary" >
            <Container className='p-0 m-1'>
                <Navbar.Brand href="#" style={{color:'white'}}>Board-Name :
                    <span className='m-1 p-1' style={{ fontWeight: '800',color:'black', backgroundColor: 'lightgray',borderRadius:'8px' }}>
                        {currentBoardName}</span></Navbar.Brand>
            </Container>
        </Navbar>
    );
}

export default BoardHeader;