import React from 'react'
import ModalCreate from './ModalCreate'
import { createListToBoardAPI, archiveListItemAPI } from './Server'
import { useParams } from 'react-router'

const CreateNewList = ({ setBoardListsData }) => {

    const { boardId } = useParams()
    const [listName, setListName] = React.useState('')
    const [show, setShow] = React.useState(false)

    function createListItem() {
        createListToBoardAPI(listName, boardId)
            .then(res => {
                setBoardListsData(prev => [...prev, res.data])
                setListName('')
            })
    }

    const onToggleShow = () => setShow(prev => !prev)

    const storeListName = (event) => setListName(event.target.value)


    return (
        <div className="m-3 primary p-0.5" style={{ width: `18rem`, backgroundColor: 'white', boxShadow: '0px 0px 2px 2px lightgray', height: '7rem' }}>
            <div className='d-flex flex-row justify-content-between align-items-center'>
                {/* <ModalCreate setBoardName={storeListName} createBoard={createListItem} name={'+ Add another list'} style={'light'} type={'list'} /> */}
                <div className='d-flex flex-column' style={{ width: "18rem", height: '4rem' }}>
                    <button type='button' className='btn btn-light m-2' onClick={onToggleShow}>{!show ? 'Add List' : 'Close tab'}</button>
                    {
                        show && <div className='d-flex flex-row p-1'>
                            <input style={{ width: '12rem' }} type='text' className='m-2' value={listName} onChange={storeListName} placeholder='enter new list name' />
                            <button style={{ width: '5rem', height: '2.4rem' }} type='button' className='btn btn-primary' onClick={createListItem}>Add</button>
                        </div>
                    }
                </div>
            </div>
        </div>
    )
}

export default CreateNewList
