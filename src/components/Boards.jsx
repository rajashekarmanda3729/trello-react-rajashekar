import React from 'react'
import { getBoards } from '../components/Server.jsx'
import Board from './Board.jsx'
import CreateNewBoard from '../components/CreateNewBoard.jsx'
import { createNewBoardAPI } from '../components/Server.jsx'
import maleImage from '../assets/icons8-male-user-50.png'
import starImage from '../assets/icons8-star-64.png'
import { randomImagesArr } from './randomImages'
import Spinner from 'react-bootstrap/Spinner';

const Boards = () => {

  const [boardsData, setBoardsData] = React.useState([])
  const [favBoardsData, setFavBoardsData] = React.useState([])
  const [isLoading, setIsLoading] = React.useState(true)


  React.useEffect(() => {
    getBoards()
      .then(res => {
        setBoardsData(res.data)
        setIsLoading(false)
      })
  }, [])

  function createNewBoard(boardName) {
    createNewBoardAPI(boardName)
      .then(res => {
        setBoardsData(prev => {
          return [...prev, res]
        })
      })
  }

  const addingToFavList = (boardItem) => {
    setFavBoardsData(prev => [...prev, boardItem])
  }

  return (
    <div className='d-flex flex-column justify-content-center'>
      <h3 className='m-3'><img src={maleImage} alt='maleImage' style={{ width: '35px' }} />  Your Boards</h3>

      {isLoading ?
        (<div className='d-flex flex-row justify-content-center align-items-center'>
          <Spinner animation="border" size="xl" />
        </div>) :
        (<div className='d-flex flex-wrap justify-content-start align-content-center'>
          {boardsData.map((eachBoard, index) => {
            return <Board boardDetails={eachBoard} key={eachBoard.id}
              bgImage={randomImagesArr[index].url} addingToFavList={addingToFavList} setBoardsData={setBoardsData} />
          })}

          <CreateNewBoard createNewBoard={createNewBoard} boardsData={boardsData} />
        </div>)}

      <div className='d-flex flex-column'>
        <h3 className='m-3'>
          <img src={starImage} alt='maleImage' style={{ width: '35px' }} />  Starred Boards
        </h3>
        <div className='d-flex flex-wrap'>
          {
            favBoardsData.map((eachBoard, index) => {
              return <Board boardDetails={eachBoard} key={eachBoard.id} bgImage={randomImagesArr[index].url} />
            })
          }
        </div>
      </div>
    </div >
  )
}

export default Boards