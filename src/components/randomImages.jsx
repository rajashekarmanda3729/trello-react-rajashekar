import { v4 as uuidv4 } from 'uuid'

export const randomImagesArr = [
    {
        id: uuidv4(),
        url: 'https://images.unsplash.com/photo-1679702173887-351a81433cdd?crop=entropy&cs=tinysrgb&fit=max&fm=jpg&ixid=Mnw3MDY2fDB8MXxjb2xsZWN0aW9ufDI3fDMxNzA5OXx8fHx8Mnx8MTY4MTE0MTc5Ng&ixlib=rb-4.0.3&q=80&w=400'
    },
    {
        id: uuidv4(),
        url: 'https://images.unsplash.com/photo-1680849488349-21138e052432?crop=entropy&cs=tinysrgb&fit=max&fm=jpg&ixid=Mnw3MDY2fDB8MXxjb2xsZWN0aW9ufDN8MzE3MDk5fHx8fHwyfHwxNjgxMTQxNzk2&ixlib=rb-4.0.3&q=80&w=400'

    },
    {
        id: uuidv4(),
        url: 'https://images.unsplash.com/photo-1680868693603-4d193a0e3ea9?crop=entropy&cs=tinysrgb&fit=max&fm=jpg&ixid=Mnw3MDY2fDB8MXxjb2xsZWN0aW9ufDV8MzE3MDk5fHx8fHwyfHwxNjgxMTQxNzk2&ixlib=rb-4.0.3&q=80&w=400'

    },
    {
        id: uuidv4(),
        url: 'https://images.unsplash.com/photo-1680894286281-350d45ca0406?crop=entropy&cs=tinysrgb&fit=max&fm=jpg&ixid=Mnw3MDY2fDB8MXxjb2xsZWN0aW9ufDZ8MzE3MDk5fHx8fHwyfHwxNjgxMTQxNzk2&ixlib=rb-4.0.3&q=80&w=400'

    },
    {
        id: uuidv4(),
        url: 'https://images.unsplash.com/photo-1680180486530-ef36f1bfc606?crop=entropy&cs=tinysrgb&fit=max&fm=jpg&ixid=Mnw3MDY2fDB8MXxjb2xsZWN0aW9ufDd8MzE3MDk5fHx8fHwyfHwxNjgxMTQxNzk2&ixlib=rb-4.0.3&q=80&w=400'

    },
    {
        id: uuidv4(),
        url: 'https://images.unsplash.com/photo-1680730134081-d805b8589ee5?crop=entropy&cs=tinysrgb&fit=max&fm=jpg&ixid=Mnw3MDY2fDB8MXxjb2xsZWN0aW9ufDJ8MzE3MDk5fHx8fHwyfHwxNjgxMTQxNzk2&ixlib=rb-4.0.3&q=80&w=400'

    },
    {
        id: uuidv4(),
        url: 'https://images.unsplash.com/photo-1680345540388-0f78ec6eaf2a?crop=entropy&cs=tinysrgb&fit=max&fm=jpg&ixid=Mnw3MDY2fDB8MXxjb2xsZWN0aW9ufDh8MzE3MDk5fHx8fHwyfHwxNjgxMTQxNzk2&ixlib=rb-4.0.3&q=80&w=400'
    },
    {
        id: uuidv4(),
        url: 'https://images.unsplash.com/photo-1680640902873-c94f3f05c168?crop=entropy&cs=tinysrgb&fit=max&fm=jpg&ixid=Mnw3MDY2fDB8MXxjb2xsZWN0aW9ufDEwfDMxNzA5OXx8fHx8Mnx8MTY4MTE0MTc5Ng&ixlib=rb-4.0.3&q=80&w=400'
    },
    {
        id: uuidv4(),
        url: 'https://images.unsplash.com/photo-1680640902873-c94f3f05c168?crop=entropy&cs=tinysrgb&fit=max&fm=jpg&ixid=Mnw3MDY2fDB8MXxjb2xsZWN0aW9ufDEwfDMxNzA5OXx8fHx8Mnx8MTY4MTE0MTc5Ng&ixlib=rb-4.0.3&q=80&w=400'
    },
    {
        id: uuidv4(),
        url: 'https://images.unsplash.com/photo-1679931974860-1af5ac3cc051?crop=entropy&cs=tinysrgb&fit=max&fm=jpg&ixid=Mnw3MDY2fDB8MXxjb2xsZWN0aW9ufDExfDMxNzA5OXx8fHx8Mnx8MTY4MTE0MTc5Ng&ixlib=rb-4.0.3&q=80&w=400'
    },
    {
        id: uuidv4(),
        url: 'https://images.unsplash.com/photo-1680631757284-617846a5ef29?crop=entropy&cs=tinysrgb&fit=max&fm=jpg&ixid=Mnw3MDY2fDB8MXxjb2xsZWN0aW9ufDEyfDMxNzA5OXx8fHx8Mnx8MTY4MTE0MTc5Ng&ixlib=rb-4.0.3&q=80&w=400'
    }
]