import React from 'react'
import { Link } from 'react-router-dom'
import deleteImage from '../assets/icons8-delete-24.png'
import { deleteBoardAPI } from './Server'
import starSelected from '../assets/starSelected.png'
import starNotSelected from '../assets/starNotSelected.png'
import Spinner from 'react-bootstrap/Spinner';
import OverlayTrigger from 'react-bootstrap/OverlayTrigger';
import Popover from 'react-bootstrap/Popover';
import Button from 'react-bootstrap/Button';


const Board = (props) => {

  const [favourite, setFavourite] = React.useState(false)
  const [loader, setLoader] = React.useState(false)

  const { name, id } = props.boardDetails
  const { bgImage, addingToFavList, setBoardsData } = props

  function deleteBoard() {
    setLoader(true)
    deleteBoardAPI(id)
      .then(res => {
        setBoardsData(prev => {
          const filterData = prev.filter(each => each.id != id)
          return filterData
        })
        setLoader(false)
      })
  }

  const settingFavourite = () => {
    setFavourite(prev => !prev)
    addingToFavList(props.boardDetails)
  }

  return (
    <div className='d-flex flex-column'>
      <div className='d-flex flex-column m-3' style={{ backgroundColor: "#FBFCF8", borderRadius: '1rem' ,boxShadow:'0PX 0PX 2PX 2PX lightgray'}} >
        <div className="card text-bg-dark" style={{ width: "18rem" }}>
          <Link to={`/boards/${id}`}>
            <img src={bgImage} className="card-img" alt="Board" style={{ height: '170px' }} />
            <div className="d-flex justify-content-between align-items-start card-img-overlay">
              <h4 className="card-title" style={{ color: 'white' }}>{name}</h4>

            </div>
          </Link>
        </div >
        <div className='d-flex flex-row justify-content-between align-items-center p-1'>
          <img src={favourite ? starSelected : starNotSelected} alt='star' style={{ height: '15px', backgroundImage: 'transparent', cursor: 'pointer' }}
            onClick={settingFavourite} />

          <OverlayTrigger
            trigger="click"
            key='right'
            placement='right'
            overlay={
              <Popover id={`popover-positioned-'right'`}>
                <Popover.Header as="h3">{name}</Popover.Header>
                <Popover.Body>
                  <div className='d-flex flex-column'>
                    <button type='button' className='btn btn-light m-2'>@Edit Name</button>
                    <button type='button' className='btn btn-light m-2'>Copy</button>
                    <button type='button' className='btn btn-light m-2'>Move</button>
                    <button type='button' className='btn btn-light m-2' onClick={deleteBoard}>@Delete</button>
                  </div>
                </Popover.Body>
              </Popover>
            }
          >
            {/* <Button variant="light" style={{ height: '1.5rem', width: '1.5rem', padding: '2px' }} >
            ...
          </Button> */}
            <h5 style={{ cursor: 'pointer' }}>...</h5>
          </OverlayTrigger>

        </div>
      </div >
      {
        loader && <div className='d-flex flex-row justify-content-center align-items-center'>
          <Spinner animation="border" size="xl" variant='danger' />
        </div>
      }
    </div>
  )
}

export default Board
