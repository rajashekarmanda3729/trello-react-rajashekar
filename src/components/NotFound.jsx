import React from 'react'

const NotFound = () => {

    return (
        <div className='d-flex flex-column justify-content-center align-items-center m-5'>
            <img
                src="https://assets.ccbp.in/frontend/react-js/not-found-blog-img.png"
                alt="not found"
                className=""
            />
            <h1 className=''>Not Found: Please check URL</h1>
        </div>
    )
}

export default NotFound
