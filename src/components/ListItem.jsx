import React from 'react'
import CreateNewCard from './CreateNewCard'
import { archiveListItemAPI } from './Server'
import Spinner from 'react-bootstrap/Spinner';
import Card from '../components/Card'
import { getCardsInLists } from '../components/Server'
import OverlayTrigger from 'react-bootstrap/OverlayTrigger';
import Popover from 'react-bootstrap/Popover';
import Button from 'react-bootstrap/Button';
import fileIcon from '../assets/icons8-copy-link-26.png'

const ListItem = ({ listDetails, setBoardListsData }) => {

    const { name, id } = listDetails

    const [listCardsList, setListCardsList] = React.useState([])
    const [archiveListLoader, setArchiveListLoader] = React.useState(false)
    const [showCard, setShowCard] = React.useState(false)
    const [cardsLoader, setCardsLoader] = React.useState(false)

    React.useEffect(() => {
        setCardsLoader(true)
        getCardsInLists(id)
            .then(res => {
                setListCardsList(res.data)
                setCardsLoader(false)
            })
    }, [name])

    const toggleCard = () => setShowCard(prev => !prev)

    function archiveListItem() {
        setArchiveListLoader(true)
        archiveListItemAPI(id)
            .then(res => {
                setBoardListsData(prev => {
                    const filterData = prev.filter(each => each.id !== res.data.id)
                    return filterData
                })
                setArchiveListLoader(false)
            })
    }

    return (
        <div className='d-flex flex-column'>
            <div className="card m-3" style={{ width: `18rem`, boxShadow: '0px 0px 2px 2px lightgray' }}>
                <div className="card-body">

                    <div className='d-flex flex-row justify-content-between align-items-center'>
                        <h5 className="card-title">{name}</h5>

                        {
                            archiveListLoader ?
                                <div className='d-flex flex-row justify-content-center align-items-center'>
                                    <Spinner animation="border" size="xl" variant='danger' />
                                </div> :
                                <OverlayTrigger
                                    trigger="click"
                                    key='right'
                                    placement='right'
                                    overlay={
                                        <Popover id={`popover-positioned-'right'`}>
                                            <Popover.Header as="h3">{name}</Popover.Header>
                                            <Popover.Body>
                                                <div className='d-flex flex-column'>
                                                    <button type='button' className='btn btn-light m-2'>@Edit Name</button>
                                                    <button type='button' className='btn btn-light m-2'>Copy</button>
                                                    <button type='button' className='btn btn-light m-2'>Move</button>
                                                    <button type='button' className='btn btn-light m-2' onClick={archiveListItem}>@Delete</button>
                                                </div>
                                            </Popover.Body>
                                        </Popover>
                                    }
                                >
                                    <Button variant="light" style={{ height: '1.5rem', width: '1.5rem', padding: '2px' }}>
                                        ...
                                    </Button>
                                </OverlayTrigger>
                        }

                    </div>
                    <div className='d-flex flex-column'>
                        {
                            (listCardsList.length != 0) ? (listCardsList.map(eachCard => {
                                return <Card cardDetails={eachCard} key={eachCard.id} setListCardsList={setListCardsList} />
                            })) : (cardsLoader && <Spinner animation="border" size="xl" variant='success' />)
                        }
                    </div>
                    {
                        !showCard && <div className='d-flex flex-row justify-content-between align-items-center p-1'>
                            <button className="btn btn-light mt-3" onClick={toggleCard}>
                                + Add a New card
                            </button>
                            <img src={fileIcon} alt='fileIcon' style={{width:'1rem'}}/>
                        </div>
                    }
                </div>
                {showCard && <CreateNewCard setShowCard={setShowCard} listId={id} setListCardsList={setListCardsList} />}
            </div>
        </div >

    )
}

export default ListItem