import React from 'react'
import { v4 as uuidv4 } from 'uuid'
import { deleteCheckItemOnCheckList, updateCheckItemOnCard } from './Server'

const CheckItem = ({ checkItemDetails, setCheckItems, checkListId, cardId }) => {
    const { id, name, state } = checkItemDetails
    const newId = uuidv4()
    const [checkboxStatus, setCheckboxStatus] = React.useState(state == 'complete' ? true : false)

    const onToggleCheckStatus = () => {
        updateCheckItemOnCard(cardId, id, state == 'complete' ? 'incomplete' : 'complete')
            .then(res => {
                setCheckboxStatus(prev => !prev)
                setCheckItems(prev => {
                    const filter = prev.filter(each => each.id != id)
                    return [...filter, res.data]
                })
            })
    }

    const onDeleteCheckItem = () => {
        deleteCheckItemOnCheckList(checkListId, id)
            .then(res => {
                setCheckItems(prev => {
                    const filter = prev.filter(each => each.id != id)
                    return filter
                })
            })
    }

    return (
        <div className='d-flex flex-row justify-content-between m-1 p-1' style={{ backgroundColor: 'lightgray', borderRadius: '1rem' }}>
            <div>
                <input type='checkbox' id={newId} className='m-2' checked={checkboxStatus} onChange={onToggleCheckStatus} />
                <label htmlFor={newId}>{`${name}`}</label>
            </div>
            <div>
                <button type="button" className='btn btn-light' onClick={onDeleteCheckItem}>...</button>
            </div>
        </div>
    )
}

export default CheckItem