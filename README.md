<h1 align="center">Trello Clone Project</h1>

<div align="center">
   Solution for a challenge from  <a href="https://trello.com/" target="_blank">Trello.com</a>.
</div>

<div align="center">
  <h3>
    <a href="https://trello-react-rajashekarmanda.netlify.app">
      Demo
    </a>
    <span> | </span>
    <a href="https://gitlab.com/rajashekarmanda3729/trello-react-rajashekar.git">
      Solution
    </a>
    <span> | </span>
    <a href="https://trello.com/">
      Challenge
    </a>
  </h3>
</div>

### UI Design 
![](src/assets/deomProject.png)


### Setup
* if you want to start project with vite@reactJS use below command
*         npm create vite@latest
*         npm install
*         npm run dev
*         npm run build

the above commans for server run/install depedencies and build code.

### ReactJS Functionalities
* Used react-components & ReactDOM 
* Used react-Bootstrap 

### Features
* User can add/delete Board & List's and checklist's

### Contact

* Github [@rajashekarmanda](https://github.com/Rajashekarmanda)


    <!-- 64367c8cbdb4f2e1e79e94c5 To learn basics -->
    <!-- 64367c8cbdb4f2e1e79e94c5 -->
    <!-- 6437accc27c7b2bb3b654aa7
    6437accc27c7b2bb3b654aa7 -->
    <!-- https://api.trello.com/1/cards/64367c8cbdb4f2e1e79e94c5/checkItem/6437accc27c7b2bb3b654aa7?state=complete&token=ATTA78d127946edd493602b647f55504494d894f03fc87ed05e9277beb1e9d072023789911ED&key=e94df84a82a9d03ca8c741197932efd4 -->

     <!-- https://api.trello.com/1/cards/64367c8cbdb4f2e1e79e94c5%7D/checkItem/6437accc27c7b2bb3b654aa7?state=incomplete&token=ATTA78d127946edd493602b647f55504494d894f03fc87ed05e9277beb1e9d072023789911ED&key=e94df84a82a9d03ca8c741197932efd4 -->